-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 07 Des 2020 pada 08.25
-- Versi server: 10.4.16-MariaDB
-- Versi PHP: 7.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kantin`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `kantin1`
--

CREATE TABLE `kantin1` (
  `id_pelanggan` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `hp` varchar(255) NOT NULL,
  `meja` int(11) NOT NULL,
  `pembayaran` varchar(255) NOT NULL,
  `menu` varchar(255) NOT NULL,
  `jumlah` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kantin1`
--

INSERT INTO `kantin1` (`id_pelanggan`, `nama`, `email`, `hp`, `meja`, `pembayaran`, `menu`, `jumlah`) VALUES
(1, 'Rahma Nur Aprilia', 'rahma@gmail.com', '087810635760', 3, 'tunai', 'pecel', 2),
(2, 'Ermadani Dyah', 'dyah@gmail.com', '08123456789', 1, 'ovo', 'rujak', 2),
(3, 'Magdalisa Anniel ', 'magdalisa@gmail.com', '08987654321', 10, 'shopee pay', 'oatmeal', 3),
(4, 'Jane Angel', 'jane@gmail.com', '08926362188', 14, 'ovo', 'bulgogi', 4),
(5, 'Alvian Rheiza', 'alvian@gmail.com', '0812398769', 5, 'kerdit card', 'Gado-gado', 2),
(6, 'Muhammad Erlangga A', 'lang@gmail.com', '08127387272', 1, 'shopee pay', 'sandwich', 2),
(7, 'Hamba Allah', 'hamba@gmail.com', '10101010', 7, 'tunai', 'pecel', 2),
(8, 'Avril Ciel B', 'avril@gmail.com', '123456789', 4, 'dana', 'Sayur Asem', 9),
(9, 'Na Jaemin', 'na.jaemin08@gmail.com', '081555442712', 19, 'dana', 'pecel', 2),
(10, 'park jisung kiyowo', 'park.jisung09@gmail.com', '081555442712', 11, 'ovo', 'pecel', 1),
(11, 'Magdalisa Anniel Miftasya', 'annnielmiftasya@gmail.com', '085334317229', 12, 'shopee pay', 'chocolatos matcha', 1),
(12, 'Magdalisa Anniel Miftasya', 'annnielmiftasya@gmail.com', '085334317229', 12, 'kerdit card', '12465898', 1),
(13, 'Rahma Elang', 'rael@gmail.com', '1029384756', 17, 'tunai', 'steak salad', 2),
(14, 'Dewina Nur Syafira', 'dewina@gmail.com', '081251995', 5, 'shopee pay', 'Sandwich', 4),
(15, 'Dewina Nur Syafira', 'dewina@gmail.com', '081251995', 5, 'shopee pay', 'Sandwich', 4),
(16, 'ermadani dyah rachmawati', 'ermadani.dyah09@gmail.com', '081555442712', 3, 'ovo', 'Pecel', 2),
(17, 'park jisung kiyowo', 'park.jisung09@gmail.com', '321971', 11, 'ovo', 'Pecel', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `kantin2`
--

CREATE TABLE `kantin2` (
  `id_pelanggan` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `email` varchar(65) NOT NULL,
  `hp` varchar(50) NOT NULL,
  `meja` int(50) NOT NULL,
  `pembayaran` varchar(50) NOT NULL,
  `menu` varchar(255) NOT NULL,
  `jumlah` int(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kantin2`
--

INSERT INTO `kantin2` (`id_pelanggan`, `nama`, `email`, `hp`, `meja`, `pembayaran`, `menu`, `jumlah`) VALUES
(1, 'jane angel', 'janeangelbrilian@gmail.com', '08165456667', 1, 'OVO', 'Spagheti', 4),
(2, 'ermadani dyah rachmawati', 'ermadani.dyah09@gmail.com', '081555442712', 19, 'ovo', 'Chicken Burger', 7),
(3, 'Na Jaemin', 'na.jaemin08@gmail.com', '081555442712', 9, 'ovo', 'Cheese Burger', 9),
(4, 'Lee jeno', 'no_no.ri_ri@gmail.com', '097636', 19, 'ovo', 'Chicken Burger', 7),
(5, 'ermadani dyah rachmawati', 'ermadani.dyah09@gmail.com', '081555442712', 11, 'ovo', 'Chicken Burger', 9),
(6, 'dyah rachmawati', 'dyahrachmawati90@gmail.com', '081555442712', 3, 'ovo', 'Chicken Burger', 7),
(7, 'Oh Sehun', 'oh_sehun09@gmail.com', '081555442712', 12, 'ovo', 'Pizza', 7);

-- --------------------------------------------------------

--
-- Struktur dari tabel `kantin3`
--

CREATE TABLE `kantin3` (
  `id_pelanggan` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `email` varchar(65) NOT NULL,
  `hp` varchar(50) NOT NULL,
  `meja` int(50) NOT NULL,
  `pembayaran` varchar(50) NOT NULL,
  `menu` varchar(255) NOT NULL,
  `jumlah` int(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `kantin3`
--

INSERT INTO `kantin3` (`id_pelanggan`, `nama`, `email`, `hp`, `meja`, `pembayaran`, `menu`, `jumlah`) VALUES
(1, 'ermadani dyah rachmawati', 'ermadani.dyah09@gmail.com', '097636', 11, 'ovo', 'Springlee', 7),
(2, 'park jisung kiyowo', 'park.jisung09@gmail.com', '08726', 12, 'ovo', 'Strudel', 1),
(3, 'park jisung kiyowo', 'park.jisung09@gmail.com', '081555442712', 12, 'ovo', 'Mattentaart', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `menu1`
--

CREATE TABLE `menu1` (
  `id_menu` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `deskripsi` varchar(255) NOT NULL,
  `harga` int(11) NOT NULL,
  `gambar` varchar(255) NOT NULL,
  `kategori` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `menu1`
--

INSERT INTO `menu1` (`id_menu`, `nama`, `deskripsi`, `harga`, `gambar`, `kategori`) VALUES
(1, 'Pecel', 'Makanan tradisional khas Jawa Timur ini berisi berbagai macam sayuran seperti bayam, kangkung, tauge, mentimun, dan banyak lagi.', 9000, 'pecel.jpeg', 'kategori1'),
(2, 'Pepes Ikan', 'Makana khas Jawa Barat dengan bantuan daun pisang untuk membungkus ikan beserta bumbunya lalu dikukus dan bisa juga dibakar', 10000, 'pepes_ikan.jpg', 'kategori1'),
(3, 'Sup Bening/ Sayur Sop', 'Kuah kaldu yang segar dan gurih dengan isian daging serta bermacam - macam sayuran', 8000, 'sup_bening.jpg', 'kategori1'),
(4, 'Gado - Gado', 'Makanan khas Indonesia yang berupa sayur-sayuran yang direbus dan dicampur menjadi satu, dengan bumbu kacang disertai irisan telur.', 10000, 'Gado_gado.jpg', 'kategori1'),
(5, 'Rujak', 'Makanan tradisional yang biasanya terbuat dari campuran berbagai macam sayuran atau buah dan dibubuhi bumbu atau kuah yang dicampuri cabe.', 10000, 'rujak.jpg', 'kategori1'),
(6, 'Sayur Asem', 'Sayur asem berbahan dasar utama jagung manis, labu siam, kacang panjang, tomat serta asem.', 8000, 'sayur_asem.jpg', 'kategori1'),
(7, 'Sandwich', 'Roti lapis yang terdiri dari sayuran, keju atau daging yang diiris, diletakkan di atas atau di antara irisan roti.', 6000, 'sandwich.png', 'kategori2'),
(8, 'Oatmeal', 'Oatmeal merupakan campuran corn meal dan pease meal yang berbentuk bubuk dengan serpihan kasar. Dicampur dengan susu dan buah - buahan.', 7000, 'oatmeal.jpg', 'kategori2'),
(9, 'Over Night Oats', 'Overnight oats adalah oatmeal yang dibuat pada malam hari dan didiamkan semalaman di kulkas. Bahan utama overnight oats adalah oats dan susu/ yogurt, lalu ditambahkan biji chia, buah, cokelat, dan sebagainya.', 7000, 'overnightoats.jpg', 'kategori2'),
(10, 'Salad Bowl', 'Salad bowl berisi kombinasi sayur-sayuran dan buah-buahan serta diberi topping mayonaise dan keju.', 8000, 'salad_bowl.jpg', 'kategori2'),
(11, 'Salad Roll', 'Kombinasi berbagai sayuran dibungkus dalam sebuah kulit tortilla ala meksiko. Kulit tortilla dapat menjadi tambahan sumber karbohidrat sebagai pemberi energi.', 9000, 'salad_roll.jpg', 'kategori2'),
(12, 'Smoothie Bowl', 'Smoothie bowl merupakan smoothie yang terbuat dari sayuran dan buah yang disajikan dalam mangkuk yang diberi taburan granola, biji-bijian, kacang-kacangan, serta potongan buah-buahan.', 12000, 'smoothie_bowl.jpg', 'kategori2'),
(13, 'Burger Vegan', 'Dengan bun yang terbuat dari gandum utuh, serta patty daging yang diganti dengan gilingan kacang-kacangan, sayur atau jamur, pas untuk memulai hidup sehat.', 12000, 'burger_vegan.jpg', 'kategori3'),
(14, 'Poke Bowl', 'Semangkuk makanan berisi potongan salmon segar dengan miso lemon, alpukat, crispy kale, wortel, wakame, edamame, dan white sushi rice.', 14000, 'poke_bowl.jpg', 'kategori3'),
(15, 'Wakame Salad', 'Wakame salad yang terdiri dari campuran aneka sayur, dan rumput laut yang disiram olive oil. Wakame dimakan dengan dua cara yaitu dalam keadaan kering dan basah.', 13000, 'wakame_salad.jpg', 'kategori3'),
(16, 'Steak Salad', 'Berisi steak daging sapi yang kaya akan sumber protein dan besi. Dengan sayur-sayuran segar yang memiliki gizi tinggi.', 16000, 'steak_salad.jpg', 'kategori3'),
(17, 'Konnyaku', 'Nasi \'Konnyaku\' goreng kecombrang dengan irisan daging lidah sapi dan telur orak-arik sebagai topping utamanya.', 15000, 'konnyaku.jpeg', 'kategori3'),
(18, 'Bulgogi', 'Bulgogi adalah makanan khas Korea yang dibumbui dengan kecap asin dan gula, serta ditemani dengan daun selada, kimchi, bawang putih, dan saus khas korea', 19000, '/bulgogi.jpg', 'kategori3');

-- --------------------------------------------------------

--
-- Struktur dari tabel `menu2`
--

CREATE TABLE `menu2` (
  `id_menu` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `deskripsi` varchar(255) NOT NULL,
  `harga` int(65) NOT NULL,
  `gambar` varchar(65) NOT NULL,
  `kategori` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `menu2`
--

INSERT INTO `menu2` (`id_menu`, `nama`, `deskripsi`, `harga`, `gambar`, `kategori`) VALUES
(1, 'Chicken Burger', 'Burger yang berisi daging ayam panggang dilengkapi dengan selada,bawang bombay dan tomat.', 10000, 'burger.jpg', 'kategori1'),
(2, 'Cheese Burger', 'Burger yang berisi daging sapi yang telah dipanggang dan dilengkapi selada, tomat, dan bawang bombay serta lapisan keju.', 12000, 'cheeseburger2.jpg', 'kategori1'),
(3, 'Fish Burger', 'Burger yang berisi daging ikan lumat yang telah digoreng menggunakan tepung dan telur.dilengkapi sayuran segar selada dan tomat, dan bawang bombay.', 12000, 'fish.jpg', 'kategori1'),
(4, 'Pizza', 'Pizza yang dipanggang dengan toping pepperoni, ham, bacon, dan nanas dilumuri dengan saus tomat', 15000, 'pizza.jpg', 'kategori1'),
(5, 'Hotdog', 'Roti bun yang berisi sosis yang telah dipanggang dan dilengkapi dengan sayuran segar\r\n												 seperti selada, tomat, dan bawang bombay.', 10000, 'hotdog.jpg', 'kategori1'),
(6, 'Nachos', 'Keripik tortilla yang disiram dengan keju cair', 8000, 'nachos.jpg', 'kategori1'),
(7, 'Tachos', 'Tortila basah yang berisi telur dadar dan kombinasi kentang, sosis dan/atau daging asap', 10000, 'tacos.jpg', 'kategori2'),
(8, 'Spaghetti', 'Pasta dengan saus bolognise dilengkapi dengan daging ayam cincang', 10000, 'sphagetti.jpg', 'kategori2'),
(9, 'Oriental Chicken Spaghetti', 'Nasi dengan potongan ayam panggang, disiram saus oriental yang pas manisnya', 8000, 'original_pasta.jpg', 'kategori2'),
(10, 'Chicken Canneloni', 'Pasta Cannelloni, Daging Ayam Cincang, Bayam, Saus Tomat, Keju Mozzarella dan Saus Krim Putih', 13000, 'canneloni.jpg', 'kategori2'),
(11, 'Beef Lasagna', 'Pasta Lasagna Panggang Berlapis Daging Sapi Cincang, Saus Krim Putih.', 15000, 'beef_lasagna.jpg', 'kategori2'),
(12, 'Thai Chicken Rice', 'Nasi bumbu gurih, chicken chunks, saus mayonnaise , sayuran.', 10000, 'thai-rice.jpg', 'kategori2'),
(13, 'Black Pepper Chicken Rice', 'Nasi dengan potongan ayam disiram saus lada hitam dan taburan wijen', 10000, 'blckpepper-rice.jpg', 'kategori3'),
(14, 'Deluxe Chicken Bruschetta', 'Bruschetta dilapisi dengan daging ayam, jamur, bawang bombay, dan saus krim putih.', 12000, 'deluxe.jpg', 'kategori3'),
(15, 'Cheese Rolls', 'Keju mozzarella gurih di dalam roti empuk dengan taburan rempah khas Italia.', 8000, 'cheese.jpg', 'kategori3'),
(16, 'Rice Omellete', 'Nasi, ayam dan paprika dalam lapisan telur omelet', 8000, 'riceom.jpg', 'kategori3'),
(17, 'Cheesy Beef Toast', 'Waffle, daging sapi asap, keju cheddar, saus krim dan potato salad', 10000, 'cheesy.jpg', 'kategori3'),
(18, 'Sandwich', 'Roti dengan isian sayuran, daging, telur, dan keju', 8000, 'sandwich.jpg', 'kategori3');

-- --------------------------------------------------------

--
-- Struktur dari tabel `menu3`
--

CREATE TABLE `menu3` (
  `id_menu` int(11) NOT NULL,
  `nama` varchar(65) NOT NULL,
  `deskripsi` varchar(255) NOT NULL,
  `harga` int(65) NOT NULL,
  `gambar` varchar(65) NOT NULL,
  `kategori` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `menu3`
--

INSERT INTO `menu3` (`id_menu`, `nama`, `deskripsi`, `harga`, `gambar`, `kategori`) VALUES
(1, 'Granola', 'Rasa Original,Honny,Green Tea', 8000, 'Optimized-100.jpg', 'kategori1'),
(2, 'Springlee', 'Rasa Original,Pedas Manis', 10000, 'Optimized-81.jpg', 'kategori1'),
(3, 'Cron Chips', 'Rasa Cheese,BBQ,Original', 5000, 'Optimized-82.jpg', 'kategori1'),
(4, 'Yorks', 'Rasa Pedas Asin,BBQ,Original,Asin', 8000, 'Optimized-83.jpg', 'kategori1'),
(5, 'Qtela', 'Singkong,Krupuk,Tempe,Ubi Ungu', 4000, 'Optimized-66.jpg', 'kategori1'),
(6, 'Piattos', 'Rasa Geprek,Matah,Rasa Geprek,Matah,Iga Penyet,BBQ,Rumput Laut,Triyaki', 6000, 'Optimized-67.jpg', 'kategori1'),
(7, 'Pocky', 'Rasa Vanila,Coklat,Strobary,Green Tea', 7000, 'Optimized-9.jpg', 'kategori1'),
(8, 'Banna Chips', 'Rasa Original,Cheese,Balado,Jagung Manis', 8000, 'Optimized-99.jpg', 'kategori1'),
(9, 'Macaron', 'Rasa Strawberry,Coconut,Chocolate,Green Tea,Matcha,Red Valvet', 10000, 'Optimized-101.jpg', 'kategori2'),
(10, 'Custard', 'Rasa Coklat,Strawberry,bluebary,Pisang,Jeruk', 12000, 'Optimized-102.jpg', 'kategori2'),
(11, 'Waffle', 'Toping Coklat,Pisang,Kacang,Madu,Strawberry,Selai,Susu', 13000, 'Optimized-103.jpg', 'kategori2'),
(12, 'Baklava', 'Isi Pisang Coklat,Coklat Kacang,Strawberry,Bluebarry,Coklat Keju', 10000, 'Optimized-104.jpg', 'kategori2'),
(13, 'Pai', 'Isi Coklat,Pisang,Keju,Strawberry,Pisang,Bluebarry,Kacang', 14000, 'Optimized-105.jpg', 'kategori2'),
(14, 'Mattentaart', 'Rasa Coklat,Keju,Blackcurrent,Bluebarry,Strawberry', 10000, 'Optimized-106.jpg', 'kategori2'),
(15, 'Strudel', 'Rasa Apel,Pisang,Nanas,Strawberry,Blackcurrent,Bluebarry', 15000, 'Optimized-107.jpg', 'kategori2'),
(16, 'Chip Cookies', 'Rasa Cheese,Vanila,Chocolate,Green tea', 9000, 'Optimized-109.jpeg', 'kategori2'),
(17, 'Klepon', 'Isi gula,Coklat,Coffee,Keju', 5000, 'Optimized-123.jpg', 'kategori3'),
(18, 'Getuk Lindri', 'Warna Merah,Hijau,Kuning', 4000, 'Optimized-124.jpg', 'kategori3'),
(19, 'Kucur', 'Rasa Gula Merah,Pandan,Strawberry', 4000, 'Optimized-125.jpg', 'kategori3'),
(20, 'Serabi', 'Kuah Santan+Gula,Santan,Gula', 6000, 'Optimized-126.jpg', 'kategori3'),
(21, 'Es Doger', 'Toping Roti,Ketan,Susu Coklat,Kacang', 7000, 'Optimized-120.jpg', 'kategori3'),
(22, 'Sekoteng', 'Toping Kacang,Kolang Kaling,Roti,Mutiara', 6000, 'Optimized-121.jpg', 'kategori3'),
(23, 'Ronde', 'Toping Kacang,Kolang Kaling,Roti,Mutiara,Ketan Hitam', 5000, 'Optimized-122.jpg', 'kategori3'),
(24, 'Boba', 'Rasa jambu,lychee,jeruk,manggo,Brown Sugar,Tai Tea', 10000, 'Optimized-110.jpg', 'kategori3'),
(25, 'Espresso Confections', 'Rasa Strawberry red velvet ,mocha ,chestnut white ,chocolate truffle', 11000, 'Optimized-111.jpg', 'kategori4'),
(26, 'Pumkin Spice Latte', 'Toping Whipped cream, Saus Karamel,Choco Chip,Chocolatee', 14000, 'Optimized-119.jpg', 'kategori4'),
(27, 'Ristretto Bianco', 'Pilih Steamed milk,Salted caramel mocha,Whipped cream, saus karamel, dan taburan gula turbinado', 15000, 'Optimized-112.jpg', 'kategori4'),
(28, 'Espresso', 'Rasa Raspberry Truffle Mocha dan Chesnut Creme Latte', 13000, 'Optimized-113.jpg', 'kategori4'),
(29, 'Caramel', 'Rasa Coffee,Chocolate,Latte', 14000, 'Optimized-1120.jpg', 'kategori4'),
(30, 'Molten', 'Rasa Tiramisu,Avocado Chocolate,Cappucino,Latte,Strawberry,Blackcurrent,		Bluebarry,Banana', 13000, 'Optimized-115.jpg', 'kategori4'),
(31, 'Mocha', 'Toping Latte,Caramel,Whipped cream,Choco Chip,Chocolate', 10000, 'Optimized-116.jpg', 'kategori4'),
(32, 'Choco Choco', 'Toping Caramel,Whipped cream, Regal,Choco Chips', 15000, 'Optimized-118.jpg', 'kategori4'),
(33, 'Le Mineral', '', 3000, 'Optimized-3.jpeg', 'kategori5'),
(34, 'Aqua', '', 3000, 'Optimized-15.jpg', 'kategori5'),
(35, 'Teh Pucuk', '', 4000, 'Optimized-47.jpg', 'kategori5'),
(36, 'Floridina', '', 3000, 'Optimized-37.jpg', 'kategori5'),
(37, 'Minute Maid Puply', '', 6000, 'Optimized-49.jpg', 'kategori5'),
(38, 'Orange Water', '', 6000, 'Optimized-63.jpg', 'kategori5'),
(39, 'Pocary Sweetr', '', 5000, 'Optimized-97.jpg', 'kategori5'),
(40, 'Tebs', '', 6000, 'Optimized-98.jpg', 'kategori5');

-- --------------------------------------------------------

--
-- Struktur dari tabel `promosi`
--

CREATE TABLE `promosi` (
  `id_menu` int(11) NOT NULL,
  `nama` varchar(65) NOT NULL,
  `deskripsi` varchar(255) NOT NULL,
  `harga` int(65) NOT NULL,
  `gambar` varchar(65) NOT NULL,
  `kantin` varchar(65) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `promosi`
--

INSERT INTO `promosi` (`id_menu`, `nama`, `deskripsi`, `harga`, `gambar`, `kantin`) VALUES
(14, 'Mattentaart', 'Mattentaart adalah cheesecake krim dan puff pastry crust yang di isi Coklat,Keju,Blackcurrent,Bluebarry,Strawberry.', 6000, '106.jpg', 'kantin3'),
(18, 'Bulgogi', 'Bulgogi dibumbui dengan kecap asin dan gula, serta ditemani dengan daun selada, kimchi, bawang putih, dan saus khas korea.', 15000, 'bulgogi.jpg', 'kantin1'),
(28, 'Espresso', 'Minuman dari hasil ekstrasi biji kopi yang sudah digiling dengan campuran rasa Raspberry Truffle Mocha atau Chesnut Creme Latte.', 8000, '113.jpg', 'kantin3'),
(32, 'Choco Choco', 'perpaduan susu segar cokelat premium olahan dari Perancis dengan toping tambahan Caramel,Whipped cream,regal', 6000, '118.jpg', 'kantin3'),
(41, 'Gado - Gado', 'Makanan khas Indonesia yang berupa sayur-sayuran yang direbus dan dicampur dengan bumbu kacang disertai irisan telur.', 7000, 'Gado_gado.jpg', 'kantin1'),
(52, 'Black Pepper Chicken Rice', 'Nasi dengan potongan ayam dan paprika disiram saus lada hitam dan taburan wijen.', 8000, 'black1.jpg', 'kantin2'),
(62, 'Tachos', 'Makanan khas Meksiko yang terdiri Tortila basah yang berisi telur dadar dan kombinasi kentang, sosis dan/atau daging asap.', 8000, 'tacos13.jpg', 'kantin2'),
(91, 'Over Night Oats', 'Bahan utama overnight,oats dan susu/ yogurt, lalu ditambahkan biji chia, buah, cokelat, dan sebagainya.', 10000, 'oat6.jpg', 'kantin1'),
(112, 'Pizza', 'Pizza yang dipanggang dengan toping pepperoni, ham, bacon,jamur,keju,tiram,Sosis dan nanas dilumuri dengan saus tomat.', 11000, 'pizza4.jpg', 'kantin2');

-- --------------------------------------------------------

--
-- Struktur dari tabel `rekomendasi`
--

CREATE TABLE `rekomendasi` (
  `id_pelanggan` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `email` varchar(65) NOT NULL,
  `hp` varchar(50) NOT NULL,
  `meja` int(50) NOT NULL,
  `pembayaran` varchar(50) NOT NULL,
  `menu` varchar(255) NOT NULL,
  `jumlah` int(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `rekomendasi`
--

INSERT INTO `rekomendasi` (`id_pelanggan`, `nama`, `email`, `hp`, `meja`, `pembayaran`, `menu`, `jumlah`) VALUES
(1, 'ermadani dyah rachmawati', 'ermadani.dyah09@gmail.com', '081555442712', 12, 'ovo', 'Oatmeal', 9),
(2, 'park jisung kiyowo', 'park.jisung09@gmail.com', '081555442712', 13, 'ovo', 'Gado - Gado', 1),
(3, 'ermadani dyah rachmawati', 'ermadani.dyah09@gmail.com', '08726', 12, 'ovo', 'Choco Choco', 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `rekomendasi_menu`
--

CREATE TABLE `rekomendasi_menu` (
  `id_menu` int(11) NOT NULL,
  `nama` varchar(65) NOT NULL,
  `deskripsi` varchar(255) NOT NULL,
  `harga` int(65) NOT NULL,
  `kategori` varchar(65) NOT NULL,
  `gambar` varchar(50) NOT NULL,
  `kantin` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `rekomendasi_menu`
--

INSERT INTO `rekomendasi_menu` (`id_menu`, `nama`, `deskripsi`, `harga`, `kategori`, `gambar`, `kantin`) VALUES
(1, 'Pecel', 'Makanan tradisional khas Jawa Timur ini berisi berbagai macam sayuran seperti bayam, kangkung, tauge, mentimun, dan banyak lagi.', 9000, 'kategori1', 'pecel.jpeg', 'kantin1'),
(2, 'Pepes Ikan', 'Makana khas Jawa Barat dengan bantuan daun pisang untuk membungkus ikan beserta bumbunya lalu dikukus dan bisa juga dibakar', 10000, 'kategori1', 'pepes_ikan.jpg', 'kantin1'),
(3, 'Sup Bening/ Sayur Sop', 'Kuah kaldu yang segar dan gurih dengan isian daging serta bermacam - macam sayuran.', 8000, 'kategori2', 'sup_bening.jpg', 'kantin1'),
(5, 'Rujak', 'Makanan tradisional yang biasanya terbuat dari campuran berbagai macam sayuran atau buah dan dibubuhi bumbu atau kuah yang dicampuri cabe.', 10000, 'kategori2', 'rujak.jpg', 'kantin1'),
(7, 'Sandwich', 'Roti lapis yang terdiri dari sayuran, keju atau daging yang diiris, diletakkan di atas atau di antara irisan roti.', 6000, 'kategori1', 'sandwich.jpg', 'kantin1'),
(8, 'Oatmeal', 'Oatmeal merupakan campuran corn meal dan pease meal yang berbentuk bubuk dengan serpihan kasar. Dicampur dengan susu dan buah - buahan.', 7000, 'kategori1', 'oatmeal.jpg', 'kantin1'),
(10, 'Salad Bowl', 'Salad bowl berisi kombinasi sayur-sayuran dan buah-buahan serta diberi topping mayonaise dan keju.', 8000, 'kategori1', 'salad_bowl.jpg', 'kantin1'),
(11, 'Waffle', 'Toping Coklat,Pisang,Kacang,Madu,Strawberry,Selai,Susu.', 6000, 'kategori3', 'Optimized-103.jpg', 'kantin3'),
(15, 'Strudel', 'Toping Kacang,Kolang Kaling,Roti,Mutiara,Ketan Hitam.', 4000, 'kategori3', 'Optimized-107.jpg', 'kantin3'),
(17, 'Poke Bowl', 'Semangkuk makanan berisi potongan salmon segar dengan miso lemon, alpukat, crispy kale, wortel, wakame, edamame, dan white sushi rice.', 14000, 'kategori1', 'poke_bowl.jpg', 'kantin1'),
(22, 'Sekoteng', 'Toping Kacang,Kolang Kaling,Roti,Mutiara.', 5000, 'kategori3', 'Optimized-121.jpg', 'kantin3'),
(23, 'Ronde', 'Toping Kacang,Kolang Kaling,Roti,Mutiara,Ketan Hitam.', 4000, 'kategori3', 'Optimized-122.jpg', 'kantin3'),
(30, 'Molten', 'Rasa Tiramisu,Avocado Chocolate,Cappucino,Latte,Strawberry,Blackcurrent,Bluebarry,Banana.', 6000, 'kategori3', 'Optimized-115.jpg', 'kantin3'),
(81, 'Sphagetti', 'Pasta dengan saus bolognise dilengkapi dengan daging ayam cincang.', 12000, 'kategori2', 'sphagetti.jpg', 'kantin2'),
(91, 'Chicken Burger', 'Burger yang berisi daging ayam panggang dilengkapi dengan selada,bawang bombay dan tomat.', 10000, 'kategori2', 'burger.jpg', 'kantin2'),
(101, 'Thai Chicken Rice', 'Nasi bumbu gurih, chicken chunks, saus mayonnaise , sayuran.', 10000, 'kategori2', 'thai-rice.jpg', 'kantin2'),
(111, 'Oriental Chicken Spaghetti', 'Nasi dengan potongan ayam panggang, disiram saus oriental yang pas manisnya.', 10000, 'kategori2', 'original_pasta.jpg', 'kantin2'),
(141, 'Pizza', 'Pizza yang dipanggang dengan toping pepperoni, ham, bacon, dan nanas dilumuri dengan saus tomat.', 15000, 'kategori3', 'pizza.jpg', 'kantin2');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id_user`, `nama`, `email`, `username`, `password`) VALUES
(1, 'Alvian Rheiza Ghata', 'rheizaalvian@gmail.com', 'allvian', 'allvian'),
(2, 'user', 'user@gmail.com', 'user', '123'),
(3, 'ermadani dyah rachmawati', 'ermadani.dyah09@gmail.com', 'ermadani_dyah', 'ermadani09'),
(4, 'park jisung kiyowo', 'park.jisung09@gmail.com', 'park_jisung', 'ermadani09'),
(5, 'Na Jaemin', 'na.jaemin08@gmail.com', 'nana_jaemin', 'ermadani09'),
(6, 'Lee jeno', 'lee.jeno09@gmail.com', 'lee_jeno', 'ermadani09'),
(7, 'Lee taeyong', 'lee.taeyoung09@gmail.com', 'lee_taeyoung', 'ermadani09');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `kantin1`
--
ALTER TABLE `kantin1`
  ADD PRIMARY KEY (`id_pelanggan`);

--
-- Indeks untuk tabel `kantin2`
--
ALTER TABLE `kantin2`
  ADD PRIMARY KEY (`id_pelanggan`);

--
-- Indeks untuk tabel `kantin3`
--
ALTER TABLE `kantin3`
  ADD PRIMARY KEY (`id_pelanggan`);

--
-- Indeks untuk tabel `menu1`
--
ALTER TABLE `menu1`
  ADD PRIMARY KEY (`id_menu`);

--
-- Indeks untuk tabel `menu2`
--
ALTER TABLE `menu2`
  ADD PRIMARY KEY (`id_menu`);

--
-- Indeks untuk tabel `menu3`
--
ALTER TABLE `menu3`
  ADD PRIMARY KEY (`id_menu`);

--
-- Indeks untuk tabel `promosi`
--
ALTER TABLE `promosi`
  ADD PRIMARY KEY (`id_menu`);

--
-- Indeks untuk tabel `rekomendasi`
--
ALTER TABLE `rekomendasi`
  ADD PRIMARY KEY (`id_pelanggan`);

--
-- Indeks untuk tabel `rekomendasi_menu`
--
ALTER TABLE `rekomendasi_menu`
  ADD PRIMARY KEY (`id_menu`);

--
-- Indeks untuk tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `kantin1`
--
ALTER TABLE `kantin1`
  MODIFY `id_pelanggan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT untuk tabel `kantin2`
--
ALTER TABLE `kantin2`
  MODIFY `id_pelanggan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `kantin3`
--
ALTER TABLE `kantin3`
  MODIFY `id_pelanggan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `menu1`
--
ALTER TABLE `menu1`
  MODIFY `id_menu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT untuk tabel `menu2`
--
ALTER TABLE `menu2`
  MODIFY `id_menu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT untuk tabel `menu3`
--
ALTER TABLE `menu3`
  MODIFY `id_menu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;

--
-- AUTO_INCREMENT untuk tabel `promosi`
--
ALTER TABLE `promosi`
  MODIFY `id_menu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=113;

--
-- AUTO_INCREMENT untuk tabel `rekomendasi`
--
ALTER TABLE `rekomendasi`
  MODIFY `id_pelanggan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `rekomendasi_menu`
--
ALTER TABLE `rekomendasi_menu`
  MODIFY `id_menu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=153;

--
-- AUTO_INCREMENT untuk tabel `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
