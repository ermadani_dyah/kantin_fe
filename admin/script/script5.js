const queryString = window.location.search
const urlParams = new URLSearchParams(queryString)
let data = urlParams.get('data');
let nilai = urlParams.get('nilai');

$.getJSON("api/promosi.php?id_menu="+nilai,function(hasil){
	$.each(hasil,function(i,data){
		console.log(data.nama);
		$('#pesanan').val(data.nama);
	});
});
$.getJSON("api/rekomendasi.php?id_menu="+data,function(hasil){
	$.each(hasil,function(i,data){
		console.log(data.nama);
		$('#pesanan').val(data.nama);
	});
});
$("#formku").submit(function(e){
	e.preventDefault();
	$("#error_nama").html('');
	$("#error_email").html('');
	$("#error_hp").html('');
	$("#error_meja").html('');
	$("#error_pembayaran").html('');
	$("#error_pesanan").html('');
	$("#error_jumlah").html('');
	
	var dataform = $("#formku").serialize();
	 $.ajax({
        url: "api/rekomendasi.php",
        type: "POST",
        data: dataform,
        success: function(result) {
			if (result.hasil == "gagal") {
                // tampilkan pesan error
				$("#error_nama").html(result.error.nama);
				$("#error_email").html(result.error.email);
				$("#error_hp").html(result.error.hp);
				$("#error_meja").html(result.error.meja);
				$("#error_pembayaran").html(result.error.pembayaran);
				$("#error_pesanan").html(result.error.pesanana);
				$("#error_jumlah").html(result.error.jumlah);
				return
            }  
                // do something, misalnya menampilkan berhasil
				$("#pesan").html("<div class=\"alert alert-success\">Data berhasil disimpan !</div>");
				window.location.replace("antrian4.html?id_antrian="+result.inserted_id);
                // kosongkan lagi error form
				$("#nama").val('');
                $("#email").val('');
				$("#hp").val('');
				$("#meja").val('');
				$("#pembayaran").val('');
				$("#pesanana").val('');
				$("#jumlah").val('');
            
        }
    });
});
