const queryString = window.location.search
const urlParams = new URLSearchParams(queryString)
let data = urlParams.get('data')
$.getJSON("api/signup.php?id_user="+data,function(hasil){
	$.each(hasil,function(i,data){
		console.log(data.nama);
		$('#username').val(data.nama);
	});
});

$("#formku").submit(function(e){
	e.preventDefault();
	$("#error_nama").html('');
	$("#error_email").html('');
	$("#error_username").html('');
	$("#error_password").html('');
	
	var dataform = $("#formku").serialize();
	 $.ajax({
        url: "api/signup.php",
        type: "POST",
        data: dataform,
        success: function(result) {
			if (result.hasil == "gagal") {
                // tampilkan pesan error
				$("#error_nama").html(result.error.nama);
				$("#error_email").html(result.error.email);
				$("#error_username").html(result.error.username);
				$("#error_password").html(result.error.password);
				return
            }  
                // do something, misalnya menampilkan berhasil
				$("#pesan").html("<div class=\"alert alert-success\">Data berhasil disimpan !</div>");
				window.location.replace("index.html");
                // kosongkan lagi error form
				$("#nama").val('');
                $("#email").val('');
				$("#username").val('');
				$("#password").val('');
            
        }
    });
});
