$.getJSON("api/rekomendasi.php",function(hasil){
	$.each(hasil,function(i,data){
		let id=data.id_menu;
		let nama=data.nama;
		let harga=data.harga;
		let gambar=data.gambar;
		let deskripsi= data.deskripsi;
		let kategori=data.kategori;
		let kantin=data.kantin;
		let pilih=""
		let content=""
		
		if(kantin=="kantin1"){
			pilih=`
				<a href="reservasi1.html?data=`+id+`" class="btn btn-primary btn-sm kantin ftco-animate" >Order Now</a>
			`
		}else if(kantin=="kantin2"){
			pilih=`
				<a href="reservasi2.html?data=`+id+`" class="btn btn-primary btn-sm kantin ftco-animate" >Order Now</a>
			`
		}else{
			pilih=`
				<a href="reservasi3.html?data=`+id+`" class="btn btn-primary btn-sm kantin ftco-animate" >Order Now</a>
			`
		}
		content=`
			<div class="media menu-item">
				<img class="mr-3" src="images/`+gambar+`" class="img-fluid" alt="Free Template by Free-Template.co">
				<div class="media-body">
					<h5 class="mt-0">`+nama+`</h5>
					<p>`+deskripsi+`</p>
					<h6 class="text-primary menu-price">Rp. `+harga+`</h6>
					<p>`+pilih+`</p>
				</div>
			</div>`
		
		if(kategori=='kategori1'){
			$('#row-kategori1').append(`
			<div class="col-md-6 ftco-animate">
				`+content+`
			</div>
		`);
		}else if (kategori=="kategori2"){
			$('#row-kategori2').append(`
			<div class="col-md-6 ftco-animate">
				`+content+`
			</div>
		`);
		}else if (kategori=="kategori3"){
			$('#row-kategori3').append(`
			<div class="col-md-6 ftco-animate">
				`+content+`
			</div>
		`);
		}
	});
});

$.getJSON("api/promosi.php",function(hasil){
	$.each(hasil,function(i,data){
		let id=data.id_menu;
		let nama=data.nama;
		let harga=data.harga;
		let gambar=data.gambar;
		let deskripsi= data.deskripsi;
		let kantin=data.kantin;
		let pilih=""
		let content=""
		
		if(kantin=="kantin1"){
			pilih=`
				<a href="reservasi1.html?data=`+id+`" class="btn btn-primary btn-sm btn-lg ftco-animate">Pemesanan</a>
			`
		}else if(kantin=="kantin2"){
			pilih=`
				<a href="reservasi2.html?data=`+id+`" class="btn btn-primary btn-sm btn-lg ftco-animate">Pemesanan</a>
			`
		}else{
			pilih=`
				<a href="reservasi3.html?data=`+id+`" class="btn btn-primary btn-sm btn-lg ftco-animate">Pemesanan</a>
			`
		}
		content=`
		<div class="item" >
			<div class="media d-block mb-4 text-center ftco-media ftco-animate border-0">
				<img src="images/`+gambar+`" alt="Free Template by Free-Template.co" class="img-fluid">
				<div class="media-body p-md-5 p-4">
					<h5 class="text-primary">Rp. `+harga+`</h5>
					<h5 class="mt-0 h4">`+nama+`</h5>
					<p class="mb-4">`+deskripsi+`</p>
					<p>`+pilih+`</p>
				</div>
			</div>
		</div>`
		$('#menu').append(content);
		
	});
});






