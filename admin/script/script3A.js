$.getJSON("api/menu2.php",function(hasil){
	$.each(hasil,function(i,data){
		let id=data.id_menu;
		let nama=data.nama;
		let harga=data.harga;
		let gambar=data.gambar;
		let deskripsi= data.deskripsi;
		let kategori=data.kategori;
		let content=""
		
		content=`
			<div class="media menu-item">
				<img class="mr-3" src="images/`+gambar+`" class="img-fluid" alt="Free Template by Free-Template.co">
				<div class="media-body">
					<h5 class="mt-0">`+nama+`</h5>
					<p>`+deskripsi+`</p>
					<h6 class="text-primary menu-price">Rp. `+harga+`</h6>
					<p>
						<a href="reservasi2.html?data=`+id+`" class="btn btn-primary btn-sm kantin ftco-animate" >Order Now</a>
					</p>
				</div>
			</div>`
		
		if(kategori=='kategori1'){
			$('#row-kategori1').append(`
			<div class="col-md-6 ftco-animate">
				`+content+`
			</div>
		`);
		}else if (kategori=="kategori2"){
			$('#row-kategori2').append(`
			<div class="col-md-6 ftco-animate">
				`+content+`
			</div>
		`);
		}else if (kategori=="kategori3"){
			$('#row-kategori3').append(`
			<div class="col-md-6 ftco-animate">
				`+content+`
			</div>
		`);
		}
	});
});



