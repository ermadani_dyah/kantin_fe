$(function () {
    $("#prosesLogin").on("click", function () {
      let username = $("#username").val()
      let password = $("#password").val()
      $.ajax({
        type: 'POST',
        url: 'api/login.php',
        data: {
          username,
          password
        },
        success: function (response) {
          if (response.success) {
            alert('Selamat, Anda berhasli login');
            localStorage.setItem("id_user", response.data.id_user)
            window.location.replace("home.html");
          } else {
            alert('Maaf data yang Anda masukkan salah');
          }
        }
      });
    });
  });
  