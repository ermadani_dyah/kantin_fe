<?php
	header('Content-Type: application/json');
	require_once '../../koneksi/koneksi.php';
	
	function view($idMenu=null){
		$koneksi=koneksi();
		if($idMenu){
			$sql = "SELECT `id_menu`,`nama`,`deskripsi`,`harga`,`kategori`,`gambar`,`kantin` from `rekomendasi_menu` where `id_menu`='$idMenu'";
		}else{
			$sql = "SELECT `id_menu`,`nama`,`deskripsi`,`harga`,`kategori`,`gambar`,`kantin` from `rekomendasi_menu`";
		}
		$query = mysqli_query($koneksi,$sql);
	
		$json_array=array();
		while($row=mysqli_fetch_assoc($query)){
			$json_array[]=$row;
		}
		return json_encode($json_array);
	}

	$metode=$_SERVER['REQUEST_METHOD'];
	if($metode=="GET"){
		if(isset($_GET['id_menu'])){
			$idMenu=$_GET['id_menu'];
			$output=view($idMenu);
		}else if(isset($_GET['id_antrian'])){
			$id_antrian = $_GET['id_antrian'];
			$output = antrian($id_antrian);
		}else{
			$output=view();
		}
	}
	echo $output;
?>