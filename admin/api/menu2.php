<?php
	header('Content-Type: application/json');
	require_once '../../koneksi/koneksi.php';
	
	function view($idMenu=null){
		$koneksi=koneksi();
		if($idMenu){
			$sql_h = "SELECT `id_menu`,`nama`,`harga`,`deskripsi`,`gambar`,`kategori` from `menu2` where `id_menu`='$idMenu'";
		}else{
			$sql_h = "SELECT `id_menu`,`nama`,`harga`,`deskripsi`,`gambar`,`kategori` from `menu2`";
		}
		$query = mysqli_query($koneksi,$sql_h);
	
		$json_array=array();
		while($row=mysqli_fetch_assoc($query)){
			$json_array[]=$row;
		}
		return json_encode($json_array);
	}
	
	function insert($nama, $email, $hp, $meja, $pembayaran, $pesanan, $jumlah){
		$koneksi=koneksi();
			
		if ($nama == '') {
			$result['error']['nama'] = 'Nama tidak boleh kosong';
		}else if ($email == '') {
			$result['error']['email'] = 'Email tidak boleh kosong';
		}else if ($hp == '') {
			$result['error']['hp'] = 'Nomor Telepon tidak boleh kosong';
		}else if ($meja == '') {
			$result['error']['meja'] = 'Nomor Meja tidak boleh kosong';
		}else if ($pembayaran == '') {
			$result['error']['pembayaran'] = 'Metode Pembayaran tidak boleh kosong';
		}else if ($pesanan == '') {
			$result['error']['pesanan'] = 'Pesanan tidak boleh kosong';
		}else if ($jumlah == '') {
			$result['error']['jumlah'] = 'jumlah tidak boleh kosong';
		}
		if (empty($result['error'])) {
			// proses simpan result 
			$sql = "insert into `kantin2` (`nama`,`email`,`hp`,`meja`,`pembayaran`,`menu`,`jumlah`) 
			VALUES ('" .$nama. "','" .$email. "','" .$hp. "','" .$meja. "','" .$pembayaran. "','" .$pesanan. "','" .$jumlah. "')";
			$query= mysqli_query($koneksi,$sql);
			if($query){
				$result['inserted_id'] =mysqli_insert_id($koneksi);
				$result['hasil'] = 'sukses';
			}else{
				$result['hasil'] = 'gagal';
			}
		} else {
			// jika validasi gagal
			$result['hasil'] = 'gagal';
		}
		// tampilkan response dalam format json
		return json_encode($result);
	}

	function antrian($id_antrian){
		$koneksi=koneksi();
		$sql = "select `id_pelanggan` from `kantin2` where `id_pelanggan`='$id_antrian'";
		$query = mysqli_query($koneksi,$sql);
		$json_array=array();
		while($row=mysqli_fetch_assoc($query)){
			$json_array[]=$row;
		}
		return json_encode($json_array);
	}

	$metode=$_SERVER['REQUEST_METHOD'];
	if($metode=="GET"){
		if(isset($_GET['id_menu'])){
			$idMenu=$_GET['id_menu'];
			$output=view($idMenu);
		}else if(isset($_GET['id_antrian'])){
			$id_antrian = $_GET['id_antrian'];
			$output = antrian($id_antrian);
		}else{
			$output=view();
		}
	}else if($metode=="POST"){
		$nama=$_POST['nama'];
		$email=$_POST['email'];
		$hp=$_POST['hp'];
		$meja=$_POST['meja'];
		$pembayaran=$_POST['pembayaran'];
		$pesanan=$_POST['pesanan'];
		$jumlah=$_POST['jumlah'];
		$output=insert($nama, $email, $hp, $meja, $pembayaran, $pesanan, $jumlah);
	}
	
	echo $output;
?>